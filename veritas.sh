#!/bin/zsh

MD5=""

while true; do
    CURRENT=`curl -H "X-LET-ME-BUY-REQUEST: justin@jdt.io" -H "X-PURPOSE: page-update-checker" -s https://veritascustomguitars.com/pro-line-hidden-page | ./pup '.product-overlay, .product-mark'`
    CURRENT_MD5=`echo $CURRENT | md5`

    echo $CURRENT >./${CURRENT_MD5}.txt

    if [[ $CURRENT_MD5 == $MD5 ]]; then
        echo "unchanged: $MD5"
    else
        echo "UPDATED: $CURRENT_MD5"
        MD5=$CURRENT_MD5
        curl -X POST https://api.twilio.com/2010-04-01/Accounts/AC1925bff9c1064629658510b14844b551/Messages.json \
            --data-urlencode "Body=PROLINE PAGE UPDATED: https://veritascustomguitars.com/pro-line-hidden-page" \
            --data-urlencode "From=+14582178891" \
            --data-urlencode "To=+1[redacted]" \
            -u AC1925bff9c1064629658510b14844b551:[redacted]
        echo
    fi
    sleep 60
done
