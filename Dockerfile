FROM alpine:latest

RUN apk update && apk upgrade && apk add curl zsh outils-md5

RUN mkdir -p /srv

WORKDIR /srv

ADD pup_v0.4.0_linux_amd64.zip /srv/pup_v0.4.0_linux_amd64.zip
RUN unzip /srv/pup_v0.4.0_linux_amd64.zip

ADD veritas.sh /srv/veritas.sh
RUN chmod 755 /srv/veritas.sh

ENTRYPOINT ["/srv/veritas.sh"]
